#!/usr/bin/make -f

build:
	docker-compose -f docker-compose.yaml -f docker-compose.pm.yaml -f docker-compose.fpm.yaml -f docker-compose.apache.yaml build --no-cache

init:
	cp symfony/.env.dist symfony/.env
	cp laravel/.env.example laravel/.env
	make build

init-db: up
	docker-compose -f docker-compose.yaml run --rm php-base symfony/bin/console d:s:u --force
	docker-compose -f docker-compose.yaml run --rm -e APP_ENV=dev php-base symfony/bin/console d:f:l -q

up:
	docker-compose -f docker-compose.yaml -f docker-compose.pm.yaml -f docker-compose.fpm.yaml -f docker-compose.apache.yaml up -d

down:
	docker-compose -f docker-compose.yaml -f docker-compose.pm.yaml -f docker-compose.fpm.yaml -f docker-compose.apache.yaml down
