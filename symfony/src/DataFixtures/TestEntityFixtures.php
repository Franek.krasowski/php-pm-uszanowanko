<?php
namespace App\DataFixtures;

use App\Entity\TestEntity;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TestEntityFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create 20 products! Bam!
        for ($i = 0; $i < 100; $i++) {
            $product = new TestEntity();
            $product->setName('name '.$i);
            $product->setProperty('prop '.$i);
            $product->setProperty2('property '.$i);
            $manager->persist($product);
        }

        $manager->flush();
    }
}
