# php-pm-uszanowanko

Project was created to test PHP-PM performance:

It requires docker, docker-compose, optionally makefile for simpler setup

You can use makefile, or run commands manually
## Simple usage
### Setup project and build containers
```make init``` 
### Rebuild containers
```make build``` 
### Start of containers
```make up```
### Kill containers
```make down```
### Create db schema and load fixtures
```make init-db```

## Exposed Ports
```
Symfony:
    81 -> ppm
    82 -> fpm
    83 -> apache
Laravel:
    91 -> ppm
    92 -> fpm
    93 -> apache
Database
    3309 -> mysql
```
# Note
This containers was created to make PHP-PM benchmarks comparing to FPM and Apache Server. It is not wise to use them to other purposes.
